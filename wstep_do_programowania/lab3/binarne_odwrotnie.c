#include <stdio.h>

int main()
{
	int n;
	int pot=1, wynik=0;
	printf("podaj liczbe \n");
	scanf("%d", &n);

	while (n!=0){

		if(n%2){
			printf("1");
			n=n-1;	
		}

		else{
			printf("0");		
		}

		n=n/2;
		wynik+=(n%2)*pot;
		pot=pot*10;
	}

	return 0;
}
