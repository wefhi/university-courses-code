#include<stdlib.h>
#include<stdio.h>
#include<time.h>

int szukaj_binarnie(int tab[20], int lewy_index, int prawy_index, int szukana){
	int srodek;
	while(lewy_index<=prawy_index){
		srodek = (lewy_index+prawy_index)/2;
		if(tab[srodek] == szukana) return srodek;
		if(tab[srodek] > szukana){ 
			prawy_index=srodek-1;
		}
		else lewy_index=srodek+1;
	}
	return -1;
}

void b_sort(int tablica[20], int ile_liczb)
{
	int temp,i,zmiana;
	do
	{
	zmiana=0;
	i=20-1;
	do
	{
	i--;
	if (tablica[i+1]< tablica[i])
	{
	temp=tablica[i];
	tablica[i]=tablica[i+1];
	tablica[i+1]=temp;
	zmiana=1;
	}
	}
	while (i!=0);
	}
	while (zmiana!=0);

	printf("\nTablica po posortowaniu:");
	for(i=0; i<20; i++) printf("%i ",tablica[i]);
	printf("\n");
}

int main()
{
	/***** LOSOWANIE TABLICY ******/
	srand( time( NULL ) );      // inicjalizacja generatora liczb pseudolosowych
	int tab[20];               // deklaracja tablicy
	for (int i=0; i<20; i++)    // dla kazdego elementu
		tab[i] = rand() % 20;   // 'wylosuj' liczbe z zakresu 0..19		   

	printf("\nTablica przed posortowaniem:");
	for(int i=0; i<20; i++) printf("%i ",tab[i]);
	printf("\n");


	b_sort(tab, 20);


	int lewy_index, prawy_index, szukana;
	printf("wpisz szukana liczbe\n");
	scanf("%d", &szukana);
	lewy_index=tab[0];
	prawy_index=tab[19];
	
	int szukaj = szukaj_binarnie(tab, 0, 19, szukana);
	if(szukaj==-1){
		printf("szukana liczba nie wystepuje\n");
	}
	else printf("pierwszy indeks na ktorej wystepuje szukana liczba to %d \n", szukaj-1);

	return 0;

}

