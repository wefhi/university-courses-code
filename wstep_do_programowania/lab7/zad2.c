#include<stdlib.h>
#include<stdio.h>
#include<time.h>

// SOROTOWANIE BĄBELKOWE
void b_sort(int tablica[20], int ile_liczb){
	int temp,i,zmiana;
	do
	{
	zmiana=0;
	i=20-1;
	do
	{
	i--;
	if (tablica[i+1]< tablica[i])
	{
	temp=tablica[i];
	tablica[i]=tablica[i+1];
	tablica[i+1]=temp;
	zmiana=1;
	}
	}
	while (i!=0);
	}
	while (zmiana!=0);

	printf("\nTablica po posortowaniu:");
	for(i=0; i<20; i++) printf("%i ",tablica[i]);
	printf("\n");
}

void wypisz_duplikaty(int tab[20]){
	for(int i=0; i<20; i++){
		int licznik=0;
		for(int i; i<20; i++){
			if(tab[i]==tab[i+1])licznik++;
			if(licznik>0)printf("liczba %d wystepuje %d razy \n", tab[i], licznik+1);		
		}	
	}
}

int main()
{
	/***** LOSOWANIE TABLICY ******/
	srand( time( NULL ) );      // inicjalizacja generatora liczb pseudolosowych
	int tab[20];               // deklaracja tablicy
	for (int i=0; i<20; i++)    // dla kazdego elementu
		tab[i] = rand() % 20;   // 'wylosuj' liczbe z zakresu 0..19		   

	printf("\nTablica przed posortowaniem:");
	for(int i=0; i<20; i++) printf("%i ",tab[i]);
	printf("\n");


	b_sort(tab, 20);


	wypisz_duplikaty(tab);

	return 0;

}