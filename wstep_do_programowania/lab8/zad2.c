#include<stdlib.h>
#include<stdio.h>


void swap(int *wartosc1,int *wartosc2);

int main()
{
	int wartosc1=1;
	int wartosc2=2;
	printf("wartosc1 to %d wartosc2 to %d \n", wartosc1, wartosc2);
	swap(&wartosc1, &wartosc2);
	printf("po zamianie wartosc1 to %d wartosc2 to %d \n", wartosc1, wartosc2);

	return 0;
}

void swap(int *wartosc1,int *wartosc2)
{
	int temp_wartosc = *wartosc1;
	*wartosc1 = *wartosc2;
	*wartosc2 = temp_wartosc;
}


