#include<stdlib.h>
#include<stdio.h>

int  tab_sum_tablica(int  tab[], int n)
{
	int max = tab[0];
	for (int i=1; i<n; i++){
		if(tab[i]>max) max=tab[i];	
	}
	return  max;
}

int tab_sum_wskaznik(int* pocz , int* kon)
{
	int max = *pocz;
	int* pt;
	for (pt = pocz; pt != kon; pt++)
		if(*pt>max) max=*pt;
	return  max;
}

int main()
{
	
	int tab[10]={10, 12, 2, 54, 2 , 1, 1, 8, 5, 10};
	int n=10;

	printf("max to: %d  \n", tab_sum_tablica(tab, n));

	printf("max to: %d  \n", tab_sum_wskaznik(&tab[0], &tab[9]));
	return 0;
}