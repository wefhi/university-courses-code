#include<time.h>
#include<stdlib.h>
#include<stdio.h>

int main()
{
	/***** LOSOWANIE TABLICY ******/
	srand( time( NULL ) );      // inicjalizacja generatora liczb pseudolosowych
	int tab[20];                // deklaracja tablicy
	for (int i=0; i<20; i++)    // dla kazdego elementu
		tab[i] = rand() % 20;   // 'wylosuj' liczbe z zakresu 0..19
	
	
	/***** Tutaj umieść rozwiązanie zadania ******/    
	int suma_prefiksowa[21];
	suma_prefiksowa[0]=0;
	printf("algorytm oblicza sume prefiksowa losowej tablicy\n");
	for (int i=0; i<20; i++){
		suma_prefiksowa[i+1]=suma_prefiksowa[i]+tab[i+1];
	}
	printf("suma prefiksowa to %d \n", suma_prefiksowa[20]);
	/***** WYŚWIETLENIE ZAWARTOŚCI TABLICY ******/    
	printf("tab = [");
	for (int i=0; i<19; i++)
		printf("%d, ", tab[i]);
	printf("%d]\n", tab[19]);
}

