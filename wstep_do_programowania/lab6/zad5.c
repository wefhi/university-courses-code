#include<time.h>
#include<stdlib.h>
#include<stdio.h>

int main()
{
	/***** LOSOWANIE TABLICY ******/
	srand( time( NULL ) );      // inicjalizacja generatora liczb pseudolosowych
	int tab[20];                // deklaracja tablicy
	for (int i=0; i<20; i++)    // dla kazdego elementu
		tab[i] = rand() % 20;   // 'wylosuj' liczbe z zakresu 0..19
	
	
	/***** Tutaj umieść rozwiązanie zadania ******/    
	int ile_el_wiekszych_od_x=0;
	int ile_el_mniejszych_od_x=0;

	int x;
	printf("sprawdz czy twoja liczba wystepuje w tablicy \n");
	scanf("%d", &x);
	for (int i=0; i<19; i++){
		if(tab[i]>x)ile_el_wiekszych_od_x++;
		if(tab[i]<x)ile_el_mniejszych_od_x++;
	}
	printf("elementow wiekszych od x bylo: %d, a elementow mniejszych od x bylo: %d \n", ile_el_wiekszych_od_x, ile_el_mniejszych_od_x);
	
	/***** WYŚWIETLENIE ZAWARTOŚCI TABLICY ******/    
	printf("tab = [");
	for (int i=0; i<19; i++)
		printf("%d, ", tab[i]);
	printf("%d]\n", tab[19]);
}

