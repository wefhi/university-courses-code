
#include <stdlib.h>
#include <stdio.h>

struct stos {
   int tab[100];
   int last_element_index;
};

void pop(struct stos stos){
    stos.last_element_index--;
}

void push(struct stos stos, int element){
    stos.last_element_index++;
}

int top(struct stos stos){
    return stos.tab[stos.last_element_index];
}

int isEmpty(struct stos stos){
    int status;
    if(stos.last_element_index==0)status=0;
    else status=1;

    return status;
}

int size(struct stos stos){
    return stos.last_element_index;
}

int main(){

    struct stos s1;
    s1.tab={1,2,3,4}; // TUTAJ JEST BŁĄD PRZY WPISYWANIU DO TABLICY
    s1.last_element_index=3;
return 0;
}
