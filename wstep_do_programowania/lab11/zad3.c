#define MAX_ROZ 100
int rozklad[MAX_ROZ];

void wyswietlRozklad(int poz);
void rozklady(int n, int poz);

int main(){
    rozklady(4, 0);
    return 0;
}

void rozklady(int n, int poz){
    if (n==0){
        wyswietlRozklad(poz);
    }
    else{
        for(int elem=1; elem<=n; elem++){
            rozklad[poz]=elem;
            rozklady(n-elem, poz+1);
        }
    }
}

void wyswietlRozklad(int poz){
    for(int i=0; i<poz; i++) printf("%d ", rozklad[i]);
    printf("\n");
}
