#include <stdio.h>

int ile_cyfr_liczby_roznych_od_0(int n) {

   if(n>0 && (n%10!=0)) return ile_cyfr_liczby_roznych_od_0(n/10)+1;
   else if(n>0) return ile_cyfr_liczby_roznych_od_0(n/10);

   return 0;
}

int  main() {
    int n=111000;
   printf("W liczbie %d jest %d cyfr roznych od zera\n",n ,ile_cyfr_liczby_roznych_od_0(n));
   return 0;
}
