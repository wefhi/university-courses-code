#include <stdio.h>

int suma_n_cyfr(int n) {
   if(n > 0) return n%10 + suma_n_cyfr(n/10);
   return 0;
}

int  main() {
   printf("Suma %d cyfr to is %d\n",12345 ,suma_n_cyfr(12345));
   return 0;
}
