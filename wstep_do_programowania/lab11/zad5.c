#include <stdio.h>

int main(){
    FILE *fin = fopen("dane.txt", "rt");
    FILE *fout = fopen("wynik.txt", "wt");

    if(fin == NULL || fout == NULL){
        printf("Blad otwarcia plikow");
    }
    else{
        char c = fgetc(fin);
        while (c != EOF){
            fputc(c, fout);
            c=fgetc(fin);
        }
    }
    if (fin!=NULL) fclose(fin);
    if (fout!=NULL) fclose(fout);

    return 0;

}
