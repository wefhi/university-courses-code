#include <stdio.h>

int main()
{
	int n;
	printf("Podaj n : ");
	scanf("%d", &n);
	for (int wiersz = 1; wiersz <= n; wiersz++){
		for (int kolumna = 1; kolumna<=n-wiersz; kolumna++)
			printf(" ");
		for (int kolumna = 1; kolumna <= 2*wiersz-1; kolumna++)
			printf("*");
		printf("\n");
	}
	return 0;
}

