#include<stdio.h>
#include<stdlib.h>
 
void swap (int *p, int *q);
void sortowanie( int tab[], int size );
int main()
{
 
    int n=6;
    int tab[]={13,12,17,13,36,71};
    sortowanie(tab, n);
    for(int x=0; x<n; x++)
    printf("%d\n", tab[x]);
    return 0;
}
void swap (int *p, int *q)
{
int c=*p;
*p=*q;
*q=c;
}
void sortowanie( int tab[], int n)
{
    int k;
    for( int i = 0; i < n-1; i++)
    {
        k = i;
        for( int j = i + 1; j < n; j++ )
        if( tab[ j ] < tab[ k ] )
             k = j;
 
        swap(&tab[k], &tab[i]);
    }
}