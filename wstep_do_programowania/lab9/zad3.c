#include <stdio.h>

void tab_show(int *tab, int n);
void swap(int *wartosc1, int *wartosc2);

int main(){

int tab[]= {1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0 , 1, 0};
int n = sizeof(tab)/sizeof(int);

int j = n - 1;
int i = 0;
tab_show(tab, n);

while(i<j){
	while(i<n && tab[i]==0) i++;
	while(i>=0 && tab[j]==1) j--;
	if(i<j) swap(&tab[i], &tab[j]);

	printf("%d %d\n", i, j);
	tab_show(tab, n);
}


return 0;
}

void swap(int *wartosc1, int *wartosc2)
{
	int temp_wartosc = *wartosc1;
	*wartosc1 = *wartosc2;
	*wartosc2 = temp_wartosc;
}

void tab_show(int *tab, int n)
{
	for(int i=0; i!=n; i++){
		printf("%d \n", tab[i]);	
	}
	printf("\n\n\n");
}