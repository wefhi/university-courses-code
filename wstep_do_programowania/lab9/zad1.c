#include <stdlib.h>
#include <stdio.h>
#include <time.h>



void pass_array_with_elements(int tab[], int n){
    for (int i=0; i<n; i++)
    {
        tab[i]=rand()%20;
	printf("%d\n", tab[i]);
    }
 
}

void pass_array_by_pointers(int* start, int* end)
{
    int* pt;
    for (pt = start; pt != end; pt++){
	*pt = rand()%20;        
	printf("%d \n",*pt);        
     }
}


int main(){

	printf("2 functions with arrays passed by array and array pointers\n");

	srand(time(NULL));

	int tab[10]={1,2,3,4,5,6,7,8,9,10};
	int n=10;

	pass_array_with_elements(tab,n);

	printf("\n\n");	

	pass_array_by_pointers(tab, tab+10);


return 0;
}